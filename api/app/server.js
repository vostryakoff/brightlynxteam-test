// const user = {name : "teset"};
// const getname = () => {
// 	const { name } = user;
// 	return name;
// }

// console.log(`name: ${getname()}`);

import app from './app';
import { PORT } from './config';

const server = app.listen(PORT, err => {
	if (err) console.log(err);	

	console.log('server running on port: ' + PORT );
});

export default server;