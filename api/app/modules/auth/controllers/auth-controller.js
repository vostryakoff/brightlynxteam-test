import pick from 'lodash/pick';
import { User } from '../../users';
import jwtService from '../../../services/jwt-service';
import { UserService } from '../../users/services';

export default {
	async signUp(ctx) {
		const userData = pick(ctx.request.body, User.createFields);
		// Заносим в базу данных пользователя или получаем ошибку валидации 
		const { _id } = await User.create(userData);
		// достанем пользователя
		const user = await UserService.getUserWhithPublicFields({ _id });

		//вернем ответ пользователю
		ctx.body = { data: user }
	},
	async signIn(ctx) {
		const { email, password } = ctx.request.body;
		// Проверим что поля существуют
		if (!email || !password) {
			ctx.throw(400, {message: 'Invalid data'});
		}
		const user = await User.findOne({ email });

		if (!user) {
			ctx.throw(400, {message: 'User not found'});
		}
		// И проверим что пароли совпадают
		if (!user.comparePasswords(password) ) {
			ctx.throw(400, {message: 'Invalid data'});
		}

		const token = await jwtService.genToken({email});
		ctx.body = { data:token }
	},
};