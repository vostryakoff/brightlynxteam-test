import { UserService } from '../services';
import pick from 'lodash/pick';

export default {
	async getCurrentUser(ctx) {
		const { user: { _id } } = ctx;

		const user = await UserService.getUserWhithPublicFields({ _id });
		ctx.body = { data : user };
	},
	async searchUsers(ctx) {
		const MAX_SIZE = 20;
		const PAGE = 1;
		const queryParams = pick(ctx.request.query, ['size', 'page']);
		const filter = {
			size: parseInt(queryParams.size),
			page: parseInt(queryParams.page),
		};
		if (!filter.size || filter.size > MAX_SIZE) {
			filter.size = MAX_SIZE;
		}
		if (!filter.page) {
			filter.page = PAGE;
		}
		const { users, ...rest } = await UserService.searchAllUserWhithPublicFields(filter);

		ctx.body = {
			data: users,
			filter,
			...rest,
		};
	}
}