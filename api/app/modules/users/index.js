import Router from 'koa-router';
import checkUser from '../../hadlers/checkUser';
import UsersController from './controllers/users-controller';
import { User } from './models';

const router = new Router({ prefix: '/users' });
router
	// Что бы обратиться к этому роуту, юзер должен быть авторизирован, проверим через middleware checkUser.
	.get('/current-user', checkUser(), UsersController.getCurrentUser)
	.get('/', UsersController.searchUsers);

export {
	User,
} 

export default router.routes();