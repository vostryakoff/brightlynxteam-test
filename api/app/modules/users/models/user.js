import mongoose, { Schema } from 'mongoose';
import bcrypt from 'bcrypt';
import uniqueValidator from 'mongoose-unique-validator';
// Подрубаем плагин
mongoose.plugin(uniqueValidator);

const UserSchema = new Schema({
	email:{
		// тип поля
		type: String,
		// Сообщение при ошибке валидации
		unique: 'User whith email "{VALUE}" already exist',
		// Приводим к нижнему регистру
		lowercase: true,
		// обязательное поле и сообщение
		required: "Email is required",
		trim: true
	},
	password: {
		type: String,
		password: "Email is required",
		trim: true
	},
	firstName: {
		type: String,
		lowercase: true,
		required: "First name is required",
		trim: true
	},
	lastName: {
		type: String,
		lowercase: true,
		required: "Last name is required",
		trim: true
	}
}, {
	// Конфигуация модели поля timestamp что бы mogoose автоматически добавил поля updated и creted
	timestamps: true
});

//  Статические параметры млжели возвращает массив полей для создания пользователя
UserSchema.statics.createFields = ["email","password","firstName", "lastName"];

// Обработчик при сохранении модели
UserSchema.pre('save', function(next) {
	if(!this.isModified('password')) {
		return next();
	}

	const salt = bcrypt.genSaltSync(10);
	this.password = bcrypt.hashSync(this.password, salt);
	next();
});

// Метод сравнения паролей
UserSchema.methods.comparePasswords = function(password) {
	return bcrypt.compareSync(password, this.password);
}


export default mongoose.model('user', UserSchema);