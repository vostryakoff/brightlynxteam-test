import { User } from '../models';

export default {
	createUser(data) {
		return User.create(data);
	},
	getUserWhithPublicFields(params) {
		return User.findOne(params).select({ password: 0, __v: 0, createdAt: 0, updatedAt: 0});
	},
	async searchAllUserWhithPublicFields({
		size,
		page,
	}) {
		// Собираем запрос для mongo
		// в query можно добавить ключи для поиска по email например
		const query = {};
		const count = await User
			.countDocuments(query)
			.sort({ updatedAt: '-1' });

		let pages = count / size;
		// Если дробное округлим
		if (pages.toString().indexOf('.') !== -1) {
			pages = parseInt(pages) + 1;
		}
		const users = await User
			.find(query)
			.sort({ updatedAt: '-1' })
			.limit(size)
			// сколько пропустить
			.skip((page - 1) * size);
		
		return {
			users,
			count,
			pages,
			page,
		};
	}
}