import mongoose from 'mongoose';

// Скажем какой тип промисов использовать
mongoose.Promise = Promise;
// Функция коннекта mongoose
export default (mongoUri) => {
	if (!mongoUri) {
		throw Error('Mongo uri is undefined');
	}
	return mongoose
		.set('useCreateIndex', true)
		.connect(mongoUri, { useNewUrlParser: true})
		.then(mongodb => {
			console.log('Mongo connected');
			return mongodb;
	});
}
