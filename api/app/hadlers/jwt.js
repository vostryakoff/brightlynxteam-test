import jwtService from '../services/jwt-service';
import { User } from '../modules/users';

export default() => async (ctx, next) => {
	// Токен будет находится в заголовке authorization
	const { authorization } = ctx.headers;
	// Если заголовок есть то мы его расхешируем, достанем email и получим пользователя по email
	if (authorization) {
		try {
			const { email } = await jwtService.verify(authorization);
			// Добавим свойство user контексту
			ctx.user = await User.findOne({ email });
		} catch(e) {
			ctx.throw(401, { message: 'Unauthorized. Invalid Token'});
		}
	}
	await next();
}