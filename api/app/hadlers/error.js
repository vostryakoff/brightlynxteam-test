export default () => async(ctx, next) => {
	try {
		await next();
	} catch ({ status = 500, message = "Server Error", name, errors }) {
		// Если это валидация mongoose то обрабатываем по другому
		if (name === 'ValidationError') {
			ctx.status = 400;
			console.log(ctx);
			// Соберем ошибки в 1 обьект
			ctx.body = {
				errors: Object.values(errors).reduce((errors, error) => ({
					...errors,
					[error.path]: error.message,
				}),{}),
			};
		} else {
			ctx.status = status;
			ctx.body = { status, message };
		}
	}
}