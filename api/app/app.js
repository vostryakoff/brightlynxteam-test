import Koa from "koa";
import initHandlers from "./hadlers";
import modules from "./modules";
import connectorsInit from "./connectors";
import cors from '@koa/cors';


const app = new Koa();
app.use(cors());


// Обработчики
initHandlers(app);
connectorsInit();
app.use(modules);


app.use(async ctx => {
	ctx.body = '<h1>Title Main</hi>';
});

export default app;