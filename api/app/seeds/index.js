import { MONGO_URI } from '../config';
import mongooseConnector from '../connectors/mongoose-connector';
import userSeeds from './user-seeds';
initSeed();
async function initSeed() {
	const mongoConnection = await mongooseConnector(MONGO_URI);
	await mongoConnection.connection.dropDatabase();
	const users = await userSeeds();
	console.log(users);

	mongoConnection.connection.close();

}