export default {
	state:  {
		userToken: null,
	},
	mutations: {
		setUser(state, payload) {
			// state.userHash = await axios.post('http://localhost:4000/api/auth/signin',  { payload.password, payload.password );	
			const { data: { data } } = payload;
			sessionStorage.setItem('userToken', data);
			state.userToken = data;
		},
		setUserToken(state, payload) {
			state.userToken = payload;
		}
	},
	actions: {
		registerUser({commit}, user) {
			commit('setUser', user);
		},
		setUserToken({commit}, token) {
			commit('setUserToken', token);
		}
	},
	getters: {
		checkUser(state) {
			return state.userToken !== null;
		},

	},
}