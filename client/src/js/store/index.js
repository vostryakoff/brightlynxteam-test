import Vue from 'vue';
import Vuex from 'vuex';
import auth from './auth';

Vue.use(Vuex);

console.log(auth);

export default new Vuex.Store(
	auth
);