import Vue from "vue";
window.Vue = require('vue');
import router from './router';
import store from './store';

import Vuelidate from 'vuelidate';
Vue.use(Vuelidate);


// Vue.component('section-title',      require("@c/ui/SectionTitle.vue").default);

// Vue.component('card-media',         require("@c/inc/CardMedia.vue").default);
// Vue.component('card-advantage',     require("@c/inc/CardAdvantage.vue").default);
// Vue.component('card-feedback',     require("@c/inc/CardFeedback.vue").default);
// Vue.component('card-timeline',     require("@c/inc/CardTimeline.vue").default);
// Vue.component('card-coach',     require("@c/inc/CardCoach.vue").default);
// Vue.component('select-city',     require("@c/inc/SelectCity.vue").default);
// Vue.component('banner-content',     require("@c/inc/BannerContent.vue").default);

// Vue.component('block',              require("@c/blocks/Block.vue").default);
// Vue.component('block-intro',        require("@c/blocks/BlockIntro.vue").default);
// Vue.component('block-media',        require("@c/blocks/BlockMedia.vue").default);
// Vue.component('block-advantage',    require("@c/blocks/BlockAdvantage.vue").default);
// Vue.component('block-feedbacks',    require("@c/blocks/BlockFeedbacks.vue").default);
// Vue.component('block-timeline',    require("@c/blocks/BlockTimeline.vue").default);
// Vue.component('block-coach',    require("@c/blocks/BlockCoaches.vue").default);
// Vue.component('banner',    require("@c/blocks/Banner.vue").default);
// Vue.component('block-map',    require("@c/blocks/Map.vue").default);

Vue.component('Vueheader', require("@c/blocks/Header.vue").default);

var app = new Vue({
	router,
	store,
    el: '#app',
    data: {
        message: 'Hello Vue!'
    },
    async created () {
    	const token = sessionStorage.getItem('userToken');
    	if (token) {
    		try {
    			await axios.post('http://localhost:4000/api/users/current-user', '', {
    				headers: { Authorization: token }
    			});
    			// this.$store.commit('setUserToken', token);
    			this.$store.dispatch('setUserToken', token);
    			this.$router.push('/');
    		} catch {

    		}
    	}
    }
});

