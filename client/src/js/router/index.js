import Vue from 'vue';
import Home from '../components/Home';
import Signin from '../components/auth/SignIn';
import Signup from '../components/auth/SignUp';
import Users from '../components/Users';
import store from '../store';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  { 
  	path: '/',
  	component: Home,
  	beforeEnter(to, from, next) {
  		store.getters.checkUser ? next() : next('signin') ;
  	}
  },
  {
  	path: '/users',
  	component: Users,
  	beforeEnter(to, from, next) {
  		store.getters.checkUser ? next() : next('signin');
  	}
  },
  { 
  	path: '/signup', component: Signup },
  { path: '/signin', component: Signin }
];


export default new VueRouter({
  routes // сокращённая запись для `routes: routes`
});